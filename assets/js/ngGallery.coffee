###
Diretiva para criar galeria de fotos
Como usar:
  1. Crie um array de objetos da seguinte forma:
    $scope.gallery = [
      { img: 'assets/images/1.jpg', description: 'Descricao da Imagem 1'}
      { img: 'assets/images/2.jpg', description: 'Descricao da Imagem 2'}
    ]

  2. Use o atributo "ng-gallery" numa DIV passando a variavel "gallery" no
  atributo "images" da seguinte forma:
    <div ng-gallery images="gallery"></div>

  3. Configuracoes
    thumbs="dots"              >> navegacao com dots
    thumbs="image"             >> navegacao com imagens
    thumbs="none"              >> sem navegacao
    thumbs-num="7"             >> qtd de thumbs na navegacao com imagens
    thumbs-class="outra-classe" >> usar classe personalizada para a galeria
    descriptions="true"        >> exibir descricao da imagem
###

angular.module('ngGallery', [ 'ngTouch' ])
.directive 'ngGallery', [
  '$document', '$timeout', '$q'
  ($document, $timeout, $q)->
    keysCodes =
      esc: 27
      left: 37
      right: 39
    {
      restrict: 'EA'
      scope:
        images:    '='
        thumbsNum: '@?'
      template: '''
        <div class="ng-gallery">
          <div class="ng-gallery-thumbs {{ thumbsClass }}">
            <div ng-repeat="i in images">
              <div style="background-image: url({{ i.img }})" class="ng-thumb" ng-click="openGallery($index)"></div>
            </div>
          </div>
          <div class="ng-gallery-content ng-overlay" ng-swipe-left="nextImage()" ng-swipe-right="prevImage()" ng-show="opened">
            <div class="ng-gallery-loading" ng-show="loading"><i class="sc-icon sc-icon-spin sc-icon-carregando-1"></i></div>
            <a class="ng-gallery-close-popup" ng-click="closeGallery()"><i class="sc-icon sc-icon-fechar-1"></i></a>
            <a class="ng-gallery-nav-left" ng-click="prevImage()"><i class="sc-icon sc-icon-seta-7-esquerda"></i></a>
            <img ng-src="{{ img }}" ng-click="nextImage()" ng-hide="loading" class="{{ loading ? 'loading' : '' }}" />
            <a class="ng-gallery-nav-right" ng-click="nextImage()"><i class="sc-icon sc-icon-seta-7-direita"></i></a>
            <span class="ng-gallery-description" ng-if="descriptions">{{ description }}</span>
            <div class="ng-thumbnails-wrapper" ng-hide="thumbs == 'none'">
              <div class="ng-thumbnails" ng-if="thumbs == 'image'">
                <div ng-repeat="i in images" style="background-image: url({{ i.img }})" class="ng-thumb" ng-class="{'active': index === $index}" ng-click="changeImage($index)"></div>
              </div>
              <div class="ng-dots" ng-if="thumbs == 'dots'">
                <span ng-repeat="i in images" class="ng-dot" ng-class="{'active': index === $index}" ng-click="changeImage($index)"></span>
              </div>
            </div>
          </div>
        </div>
        '''
      link: (scope, element, attrs)->
        $body              = $document.find('body')
        $thumbwrapper      = angular.element(document.querySelectorAll('.ng-thumbnails-wrapper'))
        $thumbnails        = -> angular.element(document.querySelectorAll('.ng-thumbnails'))
        scope.thumbs       = attrs.thumbs || 'dots'
        scope.thumbsNum    = scope.thumbsNum || 5
        scope.thumbsClass  = attrs.thumbsClass || ''
        scope.descriptions = attrs.descriptions || false
        scope.index        = 0
        scope.opened       = false
        scope.thumbsWidth  = 0

        scope.thumbsNum = 11 if scope.thumbsNum > 11

        loadImage = (i)->
          deferred = $q.defer()
          image = new Image

          image.onload = ->
            scope.loading = false
            if typeof @complete == false || @naturalWidth == 0
              deferred.reject()
            deferred.resolve image
            return

          image.onerror = ->
            deferred.reject()
            return

          image.src = scope.images[i].img
          scope.loading = true
          deferred.promise

        showImage = (i)->
          loadImage(scope.index).then (resp)->
            scope.img = resp.src
            smartScroll scope.index
            return
          scope.description = scope.images[i].description || ''
          return

        scope.changeImage = (i)->
          scope.index = i
          showImage scope.index

        scope.nextImage = ->
          scope.index += 1
          scope.index = 0 if scope.index >= scope.images.length
          showImage scope.index

        scope.prevImage = ->
          scope.index -= 1
          scope.index = scope.images.length - 1 if scope.index < 0
          showImage scope.index

        scope.openGallery = (idx)->
          unless typeof idx == undefined
            scope.index = idx
            showImage scope.index
          scope.opened = true
          if scope.thumbs == 'image'
            $timeout ->
              calculatedWidth = calculateThumbsWidth()
              scope.thumbsWidth = calculatedWidth.width
              $thumbnails().css width: calculatedWidth.width + 'px'
              $thumbwrapper.css width: calculatedWidth.visible_width + 'px'
              smartScroll scope.index
            , 100
          return

        scope.closeGallery = ->
          scope.opened = false

        navKeys = (event)->
          return unless scope.opened
          keyCode = event.which || event.keyCode

          switch keyCode
            when keysCodes.esc
              scope.closeGallery()
            when keysCodes.right
              scope.nextImage()
            when keysCodes.left
              scope.prevImage()
            else return
          scope.$apply()
          return

        $body.bind 'keydown', navKeys

        scope.$on '$destroy', ->
          $body.unbind 'keydown', navKeys

        calculateThumbsWidth = ->
          width = 0
          visible_width = 0
          angular.forEach $thumbnails().find('.ng-thumb'), (thumb)->
            width += thumb.clientWidth
            width += 10
            # margin of thumbs is 10 pixels
            visible_width = thumb.clientWidth + 10
          {
            width: width
            visible_width: visible_width * scope.thumbsNum
          }

        smartScroll = (index)->
          return unless scope.thumbs == 'image'
          item_scroll = parseInt(scope.thumbsWidth / scope.images.length, 10)
          pos = index + 1
          metade = Math.round(scope.thumbsNum / 2)
          $thumbwrapper[0].scrollLeft = pos * item_scroll - (metade * item_scroll)
        return
    }
]
