angular.module 'app', [
  'sc.app.helpers'
  'ngGallery'
]

.controller 'MainCtrl', [
  '$scope'
  ($scope)->
    $scope.images = [
      { img: 'assets/images/1.jpg'  , description: 'Descrição da Imagem 1'}
      { img: 'assets/images/2.jpg'  , description: 'Descrição da Imagem 2'}
      { img: 'assets/images/3.jpg'  , description: 'Descrição da Imagem 3'}
      { img: 'assets/images/4.jpg'  , description: 'Descrição da Imagem 4'}
      { img: 'assets/images/5.jpg'  , description: 'Descrição da Imagem 5'}
      { img: 'assets/images/6.jpg'  , description: 'Descrição da Imagem 6'}
      { img: 'assets/images/7.jpg'  , description: 'Descrição da Imagem 7'}
      { img: 'assets/images/8.jpg'  , description: 'Descrição da Imagem 8'}
      { img: 'assets/images/9.jpg'  , description: 'Descrição da Imagem 9'}
      { img: 'assets/images/10.jpg' , description: 'Descrição da Imagem 10'}
      { img: 'assets/images/11.jpg' , description: 'Descrição da Imagem 11'}
      { img: 'assets/images/12.jpg' , description: 'Descrição da Imagem 12'}
      { img: 'assets/images/13.jpg' , description: 'Descrição da Imagem 13'}
      { img: 'assets/images/14.png' , description: 'Descrição da Imagem 14'}
    ]
]
